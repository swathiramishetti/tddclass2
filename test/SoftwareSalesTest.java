import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SoftwareSalesTest {
	SoftwareSales s;

	@BeforeEach
	public void setUp() throws Exception {
		 s = new SoftwareSales();

	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void testBuyOneSWPackage() {
assertEquals(297.0, s.calculatePrice(3));	

	}
	
	@Test
	public void testBuy10SWPackage() {
assertEquals(950.4, s.calculatePrice(12));	

	}
	@Test
	public void testBuy20SWPackage() {
assertEquals(2079, s.calculatePrice(30));	

	}
	@Test
	public void testBuy50SWPackage() {
assertEquals(3564, s.calculatePrice(60));	

	}
	@Test
	public void testBuy100SWPackage() {
		assertEquals(5940, s.calculatePrice(120));	

	}
	@Test
	public void testNegativeQuantity() {
		double finalPrice = s.calculatePrice(-50);
		assertEquals(-1, finalPrice, 0);
	}
	
	@Test
	public void testZeroQuantity() {
		double finalPrice = s.calculatePrice(0);
		assertEquals(0, finalPrice,0);
	}

}
